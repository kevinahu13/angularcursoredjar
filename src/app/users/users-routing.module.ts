import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AltaComponent } from './alta/alta.component';
import { ListaComponent } from './lista/lista.component';
import { ModificacionComponent } from './modificacion/modificacion.component';

const routes: Routes = [
  {
    path:'lista',
    component: ListaComponent
  },
  {
    path:'alta',
    component: AltaComponent
  },
  {
    path:'modificacion',
    component: ModificacionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
